const mongoose = require('mongoose');
const Project = mongoose.model('Project');
const Task = mongoose.model('Task');
const User = mongoose.model('User');


const createProject = async (req, res)=> {
  try{
    project= new Project({
      name:req.body.name || '',
      user:req.user._id
    });
    await project.save();
    res.status(200).json( {
      project
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({ 
      message: error.message,
      error: {},
      title: 'error'
      });
  }
}
const listProject=  async (req, res) => {
    try{
      let arrayRes= [];
      let arrayProject = await Project.find({ user: req.user._id  }).sort({'last_update':-1});
      for (let i = 0; i < arrayProject.length; i++) {
        let project = arrayProject[i];   
        let arrayUserTime = await Task.aggregate([
          { $match: { project : project._id, user: req.user._id} },
          { $group: { _id: "$user", time: { $sum: "$time" } } }
        ]);
        let time = arrayUserTime.length ? arrayUserTime[0].time : 0;  
        let arrayUserTimeT= [];
        arrayRes.push({
          ...project.toJSON(),
          time: time
        })
      }
      res.status(200).json( {
        projects: arrayRes
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ 
        message: error.message,
        error: {},
        title: 'error'
        });
    }
  }
  
  
  const listProjectByUser = async (req, res) => {
    try{
      let project = await Project.findById( req.query._id);
      if(!project)throw  {message: 'Project does not exist error'};
      let arraytask = await Task.find({ project:project._id});
      project = project.toJSON();
      res.status(200).json( {
        project: {
            ...project,
            tasks:arraytask
        }
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ 
        message: error.message,
        error: {},
        title: 'error'
        });
    }
  }

  const listProjectAllUser = async (req, res) => {
    try{
      let arrayRes= [];
      let arrayProject = await Project.find({ user: req.user._id  }).sort({'last_update':-1});
      for (let i = 0; i < arrayProject.length; i++) {
        let project = arrayProject[i];   
        let arrayUserTime = await Task.aggregate([
          { $match: { project : project._id} },
          { $group: { _id: "$user", time: { $sum: "$time" } } }
        ]);
        let arrayUserTimeT= [];
        for (let i = 0; i < arrayUserTime.length; i++) {
          let usert = arrayUserTime[i];
          let user = await User.findById(usert._id);
          arrayUserTimeT.push({
            ...user.toJSON(),
            time: usert.time})
        }
        arrayRes.push({
          ...project.toJSON(),
          users: arrayUserTimeT
        })
      }
      res.status(200).json( {
        projects: arrayRes
      });
    } catch (error) {
      console.log(error);
      res.status(400).json({ 
        message: error.message,
        error: {},
        title: 'error'
        });
    }
  }

  module.exports = { createProject , listProject, listProjectByUser, listProjectAllUser};