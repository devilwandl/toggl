const moment = require('moment');
const mongoose = require('mongoose');
const Project = mongoose.model('Project');
const Task = mongoose.model('Task');
const TaskDetail = mongoose.model('TaskDetail');
const TaskStatus = mongoose.model('TaskStatus');




const listTask = async (req, res) => {
  try {
    let arrayTask = await Task.find({ user: req.user._id }).sort({ 'endDate': -1 });
    res.status(200).json(
      arrayTask
    );
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }
}


const getTask=  async (req, res) => {
  try {
    let task = await Task.findOne({ _id: req.query._id, user: req.user._id });
    if (!task) throw { message: 'User related task cannot be found error' };
    let taskDetail = await TaskDetail.findOne({ task: task._id, end: undefined });
    if (taskDetail) taskDetail.time = moment().diff(taskDetail.start, 'seconds');
    res.status(200).json({
      task: task,
      taskDetail: taskDetail
    });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }
}


const createTask=  async (req, res) => {
  try {
    let status = await TaskStatus.findOne({ name: "Play" });
    let task = await Task.findOne({ status: status.id, user: req.user._id });
    if (task) {
      res.status(400).json({
        message: 'Error cannot start a task because the user already has one in progress',
        _id: task._id,
        title: 'error'
      });
    } else {
      let project;
      if (req.body._id_project) {
        project = await Project.findById(req.body._id_project);
        if (!project) throw { message: "Project not found error" }
      } else {
        project = await Project.findOne({ name: '', user: req.user._id });
        if (!project) {
          project = new Project({
            name: '',
            user: req.user._id
          });
          await project.save();
        }
      }
      let task = new Task({
        name: req.body.name || '',
        user: req.user._id,
        status: status._id,
        project: project._id
      });

      await task.save();
      let taskDetail = new TaskDetail({
        task: task._id
      })
      await taskDetail.save();
      res.status(200).json({ message: "Task Started", _id: task._id });
    }
  } catch (error) {
    console.log("Failed to register task : ", error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }
}

const continueTask= async (req, res) => {
  try {
    let status = await TaskStatus.findOne({ name: "Play" });
    let task = await Task.findOne({ status: status.id, user: req.user._id });
    if (task) {
      res.status(400).json({
        message: 'Error cannot start a task because the user already has one in progress',
        _id: task._id,
        title: 'error'
      });
    } else {
      task = await Task.findById(req.body._id);
      if (!task) {
        res.status(400).json({
          message: 'task not found error',
          title: 'error'
        });
      } 
      task.status= status._id;
      await task.save();
      let taskDetail = new TaskDetail({
        task: task._id
      })
      await taskDetail.save();
      res.status(200).json({ message: "Task Started", _id: task._id });
    }
  } catch (error) {
    console.log("Failed to register task : ", error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }
}

const playTask= async (req, res) => {
  try {    
    let status = await TaskStatus.findOne({ name: "Play" });
    let task = await Task.findOne({ status: status.id, user: req.user._id });
    if (task) {
      res.status(400).json({
        message: 'Error cannot start a task because the user already has one in progress',
        _id: task._id,
        title: 'error'
      });
    } else {
      if (req.body._id) {
        task = await Task.findById(req.body._id).populate({ path: 'status', match: { name: "Stop" } });
        if (!task) throw { message: "Error that task does not exist" };
        if (!task.status) throw { message: "Error there is no task in stopped state" };
      } else {
        let project;
        if (req.body._id_project) {
          project = await Project.findById(req.body._id_project);
          if (!project) throw { message: "Project not found error" }
        } else {
          project = await Project.findOne({ name: '', user: req.user._id });
          if (!project) {
            project = new Project({
              name: '',
              user: req.user._id
            });
            await project.save();
          }
        }
        task = new Task({
          name: req.body.name || '',
          user: req.user._id,
          status: status._id,
          project: project._id
        });

      }
      task.status = status._id
      await task.save();
      let taskDetail = new TaskDetail({
        task: task._id
      })
      await taskDetail.save();
      res.status(200).json({ message: "Task Started", _id: task._id });
    }
  } catch (error) {
    console.log("Failed to register task : ", error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }

}


const stopTask =  async (req, res) => {
  try {
    let status = await TaskStatus.findOne({ name: "Play" });
    let task = await Task.findOne({ _id: req.body._id, status: status.id, user: req.user._id });
    if (!task) throw { message: 'Error the task does not exist or it is not in play state' };
    let taskDetail = await TaskDetail.findOne({ task: task._id, end: undefined });
    let project = await Project.findById(task.project);

    status = await TaskStatus.findOne({ name: "Stop" });


    let time = validateTime(req.body.time);

    project.time += time;
    await project.save();

    task.status = status._id;
    task.time += time;
    task.last_uptade = moment();
    task.endDate = moment();
    await task.save();

    taskDetail.end = moment();
    taskDetail.time = time;
    await taskDetail.save();

    res.status(200).json(task);

  } catch (error) {
    console.log("Failed to stop task : ", error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }

}


const restarTask=  async (req, res) => {
  try {
    let status = await TaskStatus.findOne({ name: "Play" });
    let task = await Task.findOne({ _id: req.body._id, status: status.id, user: req.user._id });
    if (!task) throw { message: 'Error the task does not exist or it is not in play state' };
    let taskDetail = await TaskDetail.deleteOne({ task: task._id, end: undefined });
    res.status(200).json(taskDetail);

  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }

}

const scheduledTask  = async (req, res) => {
  try {
    let project;
    let status = await TaskStatus.findOne({ name: "Stop" });
    if (req.body._id_project) {
      project = await Project.findById(req.body._id_project);
      if (!project) throw { message: "Project not found error" }
    } else {
      project = await Project.findOne({ name: '', user: req.user._id });
      if (!project) {
        project = new Project({
          name: '',
          user: req.user._id
        });
        await project.save();
      }
    }

    let time = validateTime(req.body.time);
    project.time += time;
    await project.save();

    let task = new Task({
      name: req.body.name || '',
      user: req.user._id,
      time: time,
      status: status._id,
      project: project._id,
      last_uptade: moment(),
      endDate: moment()
    });
    await task.save();
    let taskDetail = new TaskDetail({
      task: task._id,
      end: moment(),
      time: time
    })
    await taskDetail.save();
    res.status(200).json(task)
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }

}


const associateTask =  async (req, res) => {
  try {
    let project = await Project.findById(req.body._id_project);
    if (!project) throw { message: "Project not found error" }
    let arrayTask = req.body.arrayTask

    await validateTask(arrayTask);

    for (let i = 0; i < arrayTask.length; i++) {
      let task = await Task.findById(arrayTask[i]);
      let projectOld = await Project.findById(task.project);

      task.project = project.id;
      await task.save();
      project.time += task.time;
      await project.save();
      projectOld.time = projectOld.time - task.time;
      await projectOld.save();
    }
    res.status(200).json({ message: "Successful Association" });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error.message,
      error: {},
      title: 'error'
    });
  }

}



async function validateTask(arrayTask) {
  let status = await TaskStatus.findOne({ name: "Play" });
  if (!Array.isArray(arrayTask)) throw { message: "The task parameter must be an array" };
  if (!arrayTask) throw { message: "There are no tasks to associate" };
  for (let i = 0; i < arrayTask.length; i++) {
    let task = await Task.findById(arrayTask[i]);
    if (!task) throw { message: ` Error _id : ${arrayTask[i]} wrong task` };
    if (task.status == status._id) throw { message: ` Task failed _id : ${arrayTask[i]} this is in the started state` };
  }
}

function validateTime(seg) {
  if (Number.isInteger(parseInt(seg))) return seg;
  throw { message: "Error parameter time" }
}


module.exports = { listTask , getTask, createTask, continueTask, playTask, stopTask, restarTask, scheduledTask, associateTask};