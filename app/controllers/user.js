const util = require('../utility');
const sns = require('../utility/aws_sns');
const emp = require('../../config/config').app.nombre_comercial;
const aut = require('../middleware/autentication');
const moment = require('moment');
const mongoose = require('mongoose');
const User = mongoose.model('User');

const getUser = async (req, res) => {
  try{   
    res.status(200).json( {
      user:{
        name : req.user.name,
        lastname : req.user.lastname
      }
    });
  } catch (error) {
    res.status(400).json(error);
  }
}

const createUser = async (req, res) => {
  try {
    let user = new User({
      username: req.body.username,
      name: req.body.name,
      lastname: req.body.lastname,
      cell: req.body.cell
    })
    user.validate_sms_code = util.getRandomInt(1000,9999);
    user.validate_sms_date = util.getDateFive(); 
    await user.save();  
    sns.sendSMS({
      message: `Welcome to  ${emp}, ${user.validate_sms_code} this is your verification code`,
      number: String(user.cell)
    });
    res.status(200).json({message: "Registered user successfully"});
  } catch (error) {
    console.log("User registration error : ", error);
    res.status(400).json({ 
      message: error.message,
      error: {},
      title: 'error'
      });
    } 
  
};

const validateCoude =  async (req, res) => {
  try {
    let user = await User.findOne({cell: req.body.cell})
    if (!user) {
      throw { error : "User error does not exist in our System" }
    }
    validateCodeSms(user, req.body.code);
    user.validate_phone=true;
    user.save();
    sns.sendSMS({
      message: `Welcome to ${emp}, your account has been verified.`,
      number: String(user.cell)
    });
    let  token =  aut.generateToken(user);
    res.status(200).json( {
      token
    });
     
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error,
      error: {},
      title: 'error'
      });
  }
};

const resendCode = async (req, res) => {
  try {
    let user = await User.findOne({cell: req.body.cell})
    if (!user) {
      throw { error : "User error does not exist in our System" }
    }else{
      user.validate_sms_code = util.getRandomInt(1000,9999);
      user.validate_sms_date = util.getDateFive();  
      user.trycode= 0;
      await user.save();
      sns.sendSMS({
        message: `Welcome to ${emp}, ${user.validate_sms_code} This is your verification code.`,
        number: String(user.cell)
      });
      res.status(200).json();
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error,
      error: {},
      title: 'error'
      });
  }
};

function validateCodeSms(user, code){
  if(user.trycode > 5){
    throw { error : "User error has exceeded the number of code attempts" };
  }
  if(user.validate_sms_code != code){
    user.trycode = user.trycode + 1;
    user.save();
    throw { error : "Code error" };
  }
  if(!moment().diff(user.validate_sms_date, 'minutes')){
    user.trycode = user.trycode + 1;
    user.save();
    throw { error : "Expiro code error" };
  }

}

module.exports = { getUser , createUser, validateCoude , resendCode};