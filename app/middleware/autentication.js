const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const config = require('../../config/config');

exports.ensureToken=  async (req, res, next)=> {
    try {
        const bearerHeader = req.headers["authorization"];
        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader.split(" ");
            const bearerToken = bearer[1];
            //console.log("Token :", bearerToken);            
            req.token = bearerToken;
            await jwt.verify(req.token, config.jwt.my_secret_key, async (err, data) => {
                if (err) {
                    throw { message : "Token error" };
                } else {
                    req.user= await User.findOne({_id: data.user_t._id , validate_phone : true});
                    if(!req.user) {
                        throw { message : "Token error - user" };
                    }
                    return next();
                }
            });    
        } else {
            throw { message : "Token error - bearerHeader" };
        }
    } catch (error) {
        console.log(error);
        res.status(403).json({ 
            message: error.message,
            error: {},
            title: 'error' });
    }
   
  }
  

  exports.generateToken = (user)=>{
    const user_t = { _id: user._id };
    let token = jwt.sign( {user_t}, config.jwt.my_secret_key);
    return  token ;
  }