const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');

const Schema = mongoose.Schema;

const projectSchema = new Schema({
    name: {
        type: String,
        default : ""
    },
    time: {
        type: Number,
        required: [true, '{PATH} is required'],
        default: 0
    },
    creation_date: {
        type: Date,
        default: moment()
    },
    last_update: {
        type: Date,
        default: moment()
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, '{PATH} is required']
    }
});


projectSchema.plugin(uniqueValidator, { message: '{PATH} can\'t be duplicated' })

mongoose.model('Project', projectSchema);