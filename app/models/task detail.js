const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const moment = require('moment')

const Schema = mongoose.Schema;

const taskDetailSchema = new Schema({
    start: {
        type: Date,
        default: moment()
    },
    end: {
        type: Date,
        default:undefined
    },
    time: {
        type: Number
    },
    task: {
        type: Schema.Types.ObjectId,
        ref: 'Task',
        required: [true, '{PATH} is required']
    }    
});


taskDetailSchema.plugin(uniqueValidator, { message: '{PATH} can\'t be duplicated' })

mongoose.model('TaskDetail', taskDetailSchema);