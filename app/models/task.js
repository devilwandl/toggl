const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const moment = require('moment');

const Schema = mongoose.Schema;

const taskSchema = new Schema({
    name: {
        type: String
    },
    time: {
        type: Number,
        required: [true, '{PATH} is required'],
        default: 0
    },
    startDate: {
        type: Date,
        default: moment()
    },
    endDate: {
        type: Date,
        default:undefined
    },
    last_uptade: {
        type: Date,
        default: moment()
    },
    status: {
        type: Schema.Types.ObjectId,
        ref: 'TaskStatus',
        required: [true, '{PATH} is required']
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: [true, '{PATH} is required']
    }
    ,
    project: {
        type: Schema.Types.ObjectId,
        ref: 'project'
    }
    
});


taskSchema.plugin(uniqueValidator, { message: '{PATH} can\'t be duplicated' })

mongoose.model('Task', taskSchema);