const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const taskStatusSchema = new Schema({
    name: {
        type: String
    },
    creation_date: {
        type: Date,
        default: new Date()
    }    
});


taskStatusSchema.plugin(uniqueValidator, { message: '{PATH} can\'t be duplicated' })

mongoose.model('TaskStatus', taskStatusSchema);