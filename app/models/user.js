const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    username: {
        type: String,
        required: [true, '{PATH} is required']
    },
    name: {
        type: String,
        required: [true, '{PATH} is required']
    },
    lastname: {
        type: String,
        required: [true, '{PATH} is required']
    },
    cell: {
        type: String,
        unique: true,
        required: [true, '{PATH} is required'],
        minlength: 10,
        maxlength: 10
    },
    validate_phone: {
        type: Boolean,
        default: false
    },
    validate_sms_code: {
        type: Number
    },
    validate_sms_date:{
        type: Date
    },
    trycode:{
        type: Number,
        default: 0
    },
    creation_date: {
        type: Date,
        default: new Date()
    },
    last_update: {
        type: Date,
        default: new Date()
    }

    
});


userSchema.plugin(uniqueValidator, { message: '{PATH} can\'t be duplicated' })

mongoose.model('User', userSchema);