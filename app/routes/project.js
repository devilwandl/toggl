const express = require('express');
const router = express.Router();
const aut = require('../middleware/autentication');

const  { createProject , listProject, listProjectByUser, listProjectAllUser} = require('../controllers/project');


module.exports = (app) => {
    app.use('/project', router);
};

/**
 * Create project. If the name parameter is not found it will create a project with an empty name
 */
router.post('/', aut.ensureToken, createProject );

/**
 * Get the list of projects with their times
 */
router.get('/list', aut.ensureToken, listProject);

/**
 * Obtains the information of the project with its _id related to the user and its times.
 */
router.get('/', aut.ensureToken, listProjectByUser);

/**
 * all projects with user and their times are obtained
 */
router.get('/list/user', aut.ensureToken, listProjectAllUser);