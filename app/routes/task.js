const express = require('express');
const router = express.Router();
const aut = require('../middleware/autentication');

const  { listTask , getTask, createTask, continueTask,  stopTask, restarTask, scheduledTask, associateTask } = require('../controllers/task');


module.exports = (app) => {
    app.use('/task', router);
};
/**
 * Get the tasks from the most recent to the oldest.
 */
router.get('/list', aut.ensureToken, listTask);

/**
 * It obtains the data of a task from you, with the _id of the task, in the answer the taskDetail field will come null if you do not have the task in execution, 
 * but it will bring the time that has elapsed since the task started
 */
router.get('/', aut.ensureToken, getTask);

/**
 * A task can be created by omitting the project's _id, if so, a project of this user with an empty name will be searched if it does not find it, it will be created, 
 * in addition it will be possible to omit the name of the task and this will be created empty. 
 * When this task is created, it enters the play state and it will also return the _id of the task.
 */
router.post('/create', aut.ensureToken,createTask);
/**
 * Search for the task and if it is in a stopped state it will start it
 */
router.post('/continue', aut.ensureToken, continueTask);


/**
 * It allows you to stop a task that is in play state, sending it the _id of the task and its time.
 */
router.post('/stop', aut.ensureToken, stopTask);

/**
 * Restar Task
 */
router.post('/restar', aut.ensureToken,restarTask);

/**
 * allows you to schedule a task by entering the time and name of the task.
 */
router.post('/scheduled', aut.ensureToken, scheduledTask);

/**
 * allows you to associate task groups to a project.
 */
router.post('/associate', aut.ensureToken, associateTask);