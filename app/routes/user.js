const express = require('express');
const router = express.Router();
const aut = require('../middleware/autentication');

const {getUser , createUser, validateCoude , resendCode} = require('../controllers/user');


module.exports = (app) => {
    app.use('/user', router);
};

router.get('/', aut.ensureToken, getUser )

router.post('/register',  createUser );

router.post('/validatecode',  validateCoude );

router.post('/resendcode',  resendCode );

