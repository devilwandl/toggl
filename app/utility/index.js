const moment = require('moment');

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function getDateFive(){
    return moment().add(5, 'minutes');
}


module.exports = {getRandomInt, getDateFive};