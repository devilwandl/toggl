const mongoose = require('mongoose');
const TaskStatus = mongoose.model('TaskStatus');

async function initDataBase() {

    let status = await TaskStatus.findOne({ name: "Play" });
    if (!status) {
        status = new TaskStatus({
            name: 'Play'
        });
        await status.save();
    }
    status = await TaskStatus.findOne({ name: "Stop" });
    if (!status) {
        status = new TaskStatus({
            name: 'Stop'
        });
        await status.save();
    }
};

module.exports = initDataBase;
