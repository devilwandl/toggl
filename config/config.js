const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'apitoggl',
      nombre_comercial : 'toggl-emp'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost:27017/apitoggl-development',
    jwt:{
      my_secret_key : '7Zhys@mkfoiao9SAzz8j%&d&RjAzH!GFDGDFGDA^QX',
    },
    aws:{
      sns:'http://3.134.76.186:3003/sns/'
    }  
  },

  test: {
    root: rootPath,
    app: {
      name: 'apitoggl',
      nombre_comercial : 'toggl-emp'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost:27017/apitoggl-test',
    jwt:{
      my_secret_key : '7Zhys@mkfoiao9SAzz8j%&d&RjAzH!GFDGDFGDA^QX',
    },
    aws:{
      sns:'http://3.134.76.186:3003/sns/'
    }  
  },

  production: {
    root: rootPath,
    app: {
      name: 'apitoggl',
      nombre_comercial : 'toggl-emp'
    },
    port: process.env.PORT || 3000,
    db: process.env.db,
    jwt:{
      my_secret_key : process.env.jws_msk,
    },
    aws:{
      sns: process.env.sns
    }   
  }
};

module.exports = config[env];
