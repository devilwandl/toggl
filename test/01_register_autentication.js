process.env.NODE_ENV='test';

const { expect } = require('chai');
const request = require("supertest");
const app = require("../app.js");
const emp = require('../config/config').app.nombre_comercial;


const mongoose = require('mongoose');
const TaskStatus = mongoose.model('TaskStatus');
const Project = mongoose.model('Project');
const Task = mongoose.model('Task');
const TaskDetail = mongoose.model('TaskDetail');
const User = mongoose.model('User');

const USER_OBJECT = {
    username:"William",
    name:"William A.",
    lastname: "Siado T.",
    cell: 3004956158,
    code :"",
    token : ""
}

async function initial(){
    await User.deleteMany({});
    await TaskDetail.deleteMany({});
    await Task.deleteMany({});
    await Project.deleteMany({});
    
}


initial();

async function getCodeCell(num){
    let user = await User.findOne({cell:3004956158});
    return user.validate_sms_code;
}


describe("Testing =====>>>01_register_autentication",async  ()=>{
    
    describe("TEST User", ()=>{
        it("Register user - post /user/register ", done =>{
            request(app)
                .post('/user/register')
                .send(USER_OBJECT)
                .expect(200)
                .end((error , res)=>{
                    expect(res.body.message).to.equals(`Registered user successfully`);
                    done();
                })
        });
    
        
        it("Validate Code post /user/validatecode", done =>{
            getCodeCell(USER_OBJECT.cell)
                .then((code)=>{
                    USER_OBJECT.code= code;
                    let body_ = 
                        {
                            "code":USER_OBJECT.code,
                            "cell": USER_OBJECT.cell
                        }
            request(app)
                .post('/user/validatecode')
                .send(body_)
                .expect(200)
                .end((error , res)=>{
                    expect(res.body).to.have.property('token');
                    USER_OBJECT.token= "Bearer "+ res.body.token;
                    done();
                })
            })    
        })
    
        it("Resend Code post /user/resendcode", done =>{
            let body_= {
                cell: USER_OBJECT.cell
            }
            request(app)
                .post('/user/resendcode')
                .send(body_)
                .expect(200)
                .end((error , res)=>{
                    done();
                })
        })    
        

        it("Validate Code Number 2 -  post /user/validatecode", done =>{
            getCodeCell(USER_OBJECT.cell)
                .then((code)=>{
                    USER_OBJECT.code= code;
                    let body_ = 
                        {
                            "code":USER_OBJECT.code,
                            "cell": USER_OBJECT.cell
                        }
            request(app)
                .post('/user/validatecode')
                .send(body_)
                .expect(200)
                .end((error , res)=>{
                    expect(res.body).to.have.property('token');
                    expect(res.body.token).to.be.a('string');
                    USER_OBJECT.token= "Bearer "+ res.body.token;
                    done();
                })
            })    
        })
        
    })
})

