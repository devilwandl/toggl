process.env.NODE_ENV='test';

const { expect } = require('chai');
const request = require("supertest");
const app = require("../app.js");
const emp = require('../config/config').app.nombre_comercial;


const mongoose = require('mongoose');
const TaskStatus = mongoose.model('TaskStatus');
const Project = mongoose.model('Project');
const Task = mongoose.model('Task');
const TaskDetail = mongoose.model('TaskDetail');
const User = mongoose.model('User');

const USER_OBJECT = {
    username:"William",
    name:"William A.",
    lastname: "Siado T.",
    cell: 3004956158,
    code :"",
    token : ""
}


async function getCodeCell(num){
    let user = await User.findOne({cell:3004956158});
    return user.validate_sms_code;
}


describe("Testing =====>>> 02_project",async  ()=>{
    
    
    describe("TEST User", ()=>{
        it("Resend Code post /user/resendcode", done =>{
            let body_= {
                cell: USER_OBJECT.cell
            }
            request(app)
                .post('/user/resendcode')
                .send(body_)
                .expect(200)
                .end((error , res)=>{
                    done();
                })
        })  
                
        it("Validar Code post /user/validatecode", done =>{
            getCodeCell(USER_OBJECT.cell)
                .then((code)=>{
                    USER_OBJECT.code= code;
                    let body_ = 
                        {
                            "code":USER_OBJECT.code,
                            "cell": USER_OBJECT.cell
                        }
            request(app)
                .post('/user/validatecode')
                .send(body_)
                .expect(200)
                .end((error , res)=>{
                    expect(res.body).to.have.property('token');
                    expect(res.body.token).to.be.a('string');
                    USER_OBJECT.token= "Bearer "+ res.body.token;
                    done();
                })
            })    
        })        
    })


    describe("TEST Project", ()=>{
        let arrayProjects= [];
        it("Create project : not parameters - Post /project ", done =>{
            let body_= {
                };
            request(app)
                .post('/project/')
                .set({Authorization: USER_OBJECT.token})
                .send(body_)
                .expect(200)
                .end((error , res)=>{
                    expect(res.body).to.have.property('project');
                    arrayProjects.push(res.body.project);
                    done();
                })
    
        })
        
        it("Create project - Post /project ", done =>{
            let body_= {
                name: "Project - 1"
                };
            request(app)
                .post('/project/')
                .set({Authorization: USER_OBJECT.token})
                .send(body_)
                .expect(200)
                .end((error , res)=>{
                    expect(res.body).to.have.property('project');
                    arrayProjects.push(res.body.project);
                    done();
                })
    
        })
        
        it("Get project id - Get /project ", done =>{
            let params= {
                _id: arrayProjects[0]._id
                };
            request(app)
                .get('/project/')
                .set({Authorization: USER_OBJECT.token})
                .query(params)
                .expect(200)
                .end((error , res)=>{
                    console.log(res.body.project._id);
                    expect(res.body).to.have.property('project');
                    expect(res.body.project._id).to.equals(arrayProjects[0]._id);
                    done();
                })
    
        })

        
        it("Get project - /project/list ", done =>{
            request(app)
                .get('/project/list/')
                .set({Authorization: USER_OBJECT.token})
                .query()
                .expect(200)
                .end((error , res)=>{
                    expect(res.body).to.have.property('projects');
                    expect(res.body).to.have.property('projects').with.lengthOf(2);                   
                    done();
                })
    
        })

        it("Get project User - /project/list/user ", done =>{
            request(app)
                .get('/project/list/user')
                .set({Authorization: USER_OBJECT.token})
                .query()
                .expect(200)
                .end((error , res)=>{
                    expect(res.body).to.have.property('projects');
                    expect(res.body).to.have.property('projects').with.lengthOf(2);                   
                    done();
                })
    
        })
    })
})

