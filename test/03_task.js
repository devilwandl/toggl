process.env.NODE_ENV = 'test';

const { expect } = require('chai');
const request = require("supertest");
const app = require("../app.js");
const emp = require('../config/config').app.nombre_comercial;


const mongoose = require('mongoose');
const TaskStatus = mongoose.model('TaskStatus');
const Project = mongoose.model('Project');
const Task = mongoose.model('Task');
const TaskDetail = mongoose.model('TaskDetail');
const User = mongoose.model('User');

const USER_OBJECT = {
    username: "William",
    name: "William A.",
    lastname: "Siado T.",
    cell: 3004956158,
    code: "",
    token: ""
}





async function getCodeCell(num) {
    let user = await User.findOne({ cell: 3004956158 });
    return user.validate_sms_code;
}


describe("Testing =====>>> 03_task", async () => {


    describe("TEST User", () => {
        it("Resend Code post /user/resendcode", done => {
            let body_ = {
                cell: USER_OBJECT.cell
            }
            request(app)
                .post('/user/resendcode')
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    done();
                })
        })

        it("Validar Code post /user/validatecode", done => {
            getCodeCell(USER_OBJECT.cell)
                .then((code) => {
                    USER_OBJECT.code = code;
                    let body_ =
                    {
                        "code": USER_OBJECT.code,
                        "cell": USER_OBJECT.cell
                    }
                    request(app)
                        .post('/user/validatecode')
                        .send(body_)
                        .expect(200)
                        .end((error, res) => {
                            expect(res.body).to.have.property('token');
                            expect(res.body.token).to.be.a('string');
                            USER_OBJECT.token = "Bearer " + res.body.token;
                            done();
                        })
                })
        })
    })


    describe("TEST Task", () => {
        let arrayTask = [];
        let taskId;

        it("Create Task : not parameters - task/create ", done => {
            let body_ = {
            };
            request(app)
                .post('/task/create')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('_id');
                    taskId= res.body._id;
                    done()
                })

        })

        it("Stop Task   - task/stop", done => {
            let body_ = {
                "_id": taskId,
                "time": 1
            };
            request(app)
                .post('/task/stop')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('_id');
                    expect(res.body).to.have.property('time');
                    expect(res.body._id).equals(taskId);
                    expect(res.body.time).equals(1);
                    done()
                })

        })
       
        it("Continue Task - task/continue", done => {
            let body_ = {
                _id: taskId
            };
            request(app)
                .post('/task/continue')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('_id');
                    expect(res.body._id).equals(taskId);
                    done()
                })

        })

        it("Stop Task :  - task/stop", done => {
            let body_ = {
                "_id": taskId,
                "time": 1
            };
            request(app)
                .post('/task/stop')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('_id');
                    expect(res.body).to.have.property('time');
                    expect(res.body._id).equals(taskId);
                    expect(res.body.time).equals(2);
                    done()
                })
        })      
        
        it("Get list task - task/list ", done => {
            request(app)
                .get('/task/list')
                .set({ Authorization: USER_OBJECT.token })
                .expect(200)
                .end((error, res) => {
                    expect(res.body).with.lengthOf(1);   
                    expect(res.body[0]).to.have.property('_id');
                    expect(res.body[0]).to.have.property('time');
                    expect(res.body[0]._id).equals(taskId);
                    expect(res.body[0].time).equals(2);
                    arrayTask= res.body;
                    done()  
                })

        })

        it("Create Task  - task/create ", done => {
            let body_ = {
            
                name: "Task number - 3",
                _id_project: arrayTask[0]._id_project
            
            };
            request(app)
                .post('/task/create')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('_id');
                    taskId= res.body._id;
                    done()
                })
        })

        it("Stop Task :  - task/stop", done => {
            let body_ = {
                "_id": taskId,
                "time": 1
            };
            request(app)
                .post('/task/stop')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('_id');
                    expect(res.body).to.have.property('time');
                    expect(res.body._id).equals(taskId);
                    expect(res.body.time).equals(1);
                    done()
                })

        })

        it("Get task id - task/ ", done => {
            let params= {
                _id: arrayTask[0]._id
                };
            request(app)
                .get('/task/')
                .set({ Authorization: USER_OBJECT.token })
                .query(params)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('task');
                    expect(res.body).to.have.property('taskDetail');
                    expect(res.body.task).to.have.property('_id');
                    expect(res.body.task).to.have.property('time');
                    expect(res.body.task.time).equals(2);
                    done()  
                })

        })

        it("Scheduled Task - task/scheduled ", done => {
            let body_ = {
                name: "scheduled Task - 1",
                time: 2
            };
            request(app)
                .post('/task/scheduled')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {

                    expect(res.body).to.have.property('_id');
                    expect(res.body).to.have.property('time');
                    expect(res.body.time).equals(2);
                    taskId= res.body._id;
                    done();
                })
        })
        
        it("Get list task - task/list ", done => {
            request(app)
                .get('/task/list')
                .set({ Authorization: USER_OBJECT.token })
                .expect(200)
                .end((error, res) => {
                    let time_=0;
                    expect(res.body).with.lengthOf(3);   
                    res.body.forEach(task => {
                        time_ +=task.time;
                    });
                    expect(time_).equals(5);
                    done()  
                })
        })

        it("Scheduled Task - task/associate ", done => {
            let arrayTask_= arrayTask.map(task => {
                return task._id;
            });
            let body_ = {
                _id_project: arrayTask[0].project,
                arrayTask : arrayTask_
            };
            request(app)
                .post('/task/associate')
                .set({ Authorization: USER_OBJECT.token })
                .send(body_)
                .expect(200)
                .end((error, res) => {
                    expect(res.body).to.have.property('message');
                    expect(res.body.message).equals('Successful Association');
                    done();
                })
        })
        
        
        
    })
})

